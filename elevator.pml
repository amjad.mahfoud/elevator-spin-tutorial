#define NumberOfFloors 6
mtype={Moving, Stopped, Opened, Closed};
typedef Car{
	int id;	
	mtype state;// Stoped; Moving 
	mtype door; // Closed; Opend
	int CF // Current floor
 }
	
typedef token 
{
	Car c;
	int Fd	
}

chan TravelRequestRegistred= [26] of {token};
chan FloorButtonPressed= [NumberOfFloors] of {token};
chan HallButtonPresed= [(NumberOfFloors-1)*2] of {int}; // from 1 to NumberOfFloors
chan MoveToNextFloor=[2] of {token};
chan CarStopped= [2] of {token};
chan DoorOpened=[2] of {token};
chan DoorClosed=[2] of {token};
typedef mapCartoRequest
{
	int cid;
	int fid;
}
chan CarSelected= [(NumberOfFloors-1)*2] of {mapCartoRequest};

int SelectedCarNumber;
	
init
{

/*********  Initial Marking  *********/
		
	token Cars[2]; 	
	
	/***** car one  *****/
	
	Cars[0].c.id=1;
	Cars[0].c.state=Stopped;
	Cars[0].c.door=Closed;
	Cars[0].c.CF=3;
	Cars[0].Fd=4;
	FloorButtonPressed!Cars[0];

	/***** car tow  *****/
	
	Cars[1].c.id=2;
	Cars[1].c.state=Stopped;
	Cars[1].c.door=Opened;
	Cars[1].c.CF=1;
	Cars[1].Fd=6;
	FloorButtonPressed!Cars[1];

	HallButtonPresed!5;
	HallButtonPresed!6;

	
/*********  End Of Initialization  *********/	

                token temp; // to hold requests temporarly
	
       	 int fd;
	int numElements;
	mapCartoRequest map;

	do
/*t1*/	:: atomic{ 
			
			(len(HallButtonPresed)>0) -> 
			if  // choose randomly a car to serve new request
			::SelectedCarNumber=1 // car one selected
			::SelectedCarNumber=2 // car two selected
			fi;
			map.cid=SelectedCarNumber;
			HallButtonPresed?map.fid;
			CarSelected!map;
			printf("Car number %d selected to serve request\n",map.cid)}

/*t2*/	:: atomic{
		(len(CarSelected)>0) ->
		CarSelected?map;
		 Cars[map.cid - 1].Fd=map.fid; 			
		TravelRequestRegistred!Cars[map.cid - 1]			
	}

/*t3*/	:: atomic{
		(len(FloorButtonPressed)>0) ->
		FloorButtonPressed?temp;
		//len(FloorButtonPressed);
		TravelRequestRegistred!temp
	}
	::atomic{
		(len(TravelRequestRegistred)>0) ->
		TravelRequestRegistred?temp;
		if
	/*t4*/	:: (Cars[temp.c.id - 1].c.state == Stopped) && (Cars[temp.c.id - 1].c.door==Opened) ->
			Cars[temp.c.id - 1].c.door=Closed;
			DoorClosed!Cars[temp.c.id - 1];
			printf("Car %d Door Closed", temp.c.id)
	/*t5*/	:: (Cars[temp.c.id - 1].c.state == Stopped) && (Cars[temp.c.id - 1].c.door==Closed)  && (Cars[temp.c.id - 1].c.CF != temp.Fd) ->
			if
			:: (Cars[temp.c.id - 1].c.CF < Cars[temp.c.id - 1].Fd) -> Cars[temp.c.id - 1].c.CF=Cars[temp.c.id - 1].c.CF + 1; Cars[temp.c.id -1].c.state= Moving// move up
			:: (Cars[temp.c.id - 1].c.CF > Cars[temp.c.id - 1].Fd) -> Cars[temp.c.id - 1].c.CF=Cars[temp.c.id - 1].c.CF - 1; Cars[temp.c.id -1].c.state= Moving // move down
			fi
			printf("Car %d moving", temp.c.id)
	/*t6*/	:: (Cars[temp.c.id - 1].c.CF == temp.Fd)  ->
			Cars[temp.c.id - 1].c.door=Opened;
			DoorOpened!Cars[temp.c.id - 1];
			printf("Car %d Door Opened", temp.c.id)
		fi
		
	}
/*t7*/	:: atomic{
		(len(DoorClosed)>0) ->
		DoorClosed?temp;
		if
		:: (Cars[temp.c.id - 1].c.CF != Cars[temp.c.id - 1].Fd) ->			
		     if
		     :: (Cars[temp.c.id - 1].c.CF < Cars[temp.c.id - 1].Fd) -> Cars[temp.c.id - 1].c.CF=Cars[temp.c.id - 1].c.CF + 1; Cars[temp.c.id -1].c.state= Moving// move up
		     :: (Cars[temp.c.id - 1].c.CF > Cars[temp.c.id - 1].Fd) -> Cars[temp.c.id - 1].c.CF=Cars[temp.c.id - 1].c.CF - 1; Cars[temp.c.id -1].c.state= Moving // move down
		      fi
		fi
		MoveToNextFloor!Cars[temp.c.id - 1]
	}
/*t8 t9*/	:: atomic{
		(len(MoveToNextFloor)>0) ->
		MoveToNextFloor?temp;
	/*	if */
	/*t8*/	/*::*/
	/*t9*/	/*::*/
	/*	fi */
		
	}
/*t10*/	:: atomic{
	(len(CarStopped) > 0) ->
	CarStopped?temp;
	Cars[temp.c.id -1].c.door=Opened;
	DoorOpened!Cars[temp.c.id -1]
	}
/*t11*/	:: atomic{
	(len(DoorOpened) > 0) ->
	DoorOpened?temp;
	Cars[temp.c.id -1].c.door=Closed;
	DoorClosed!Cars[temp.c.id -1]
	}
	od
}















